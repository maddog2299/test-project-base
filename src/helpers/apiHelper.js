'use strict';

import config from '../configs/config';

export default {

  apiRequest (path, reqParams) {
    return fetch(config.apiUrl + path, reqParams)
        .then(res => {
          if (res.ok) return res;
          return res.text().then(text => {
            throw new Error('error ' + res.status + '\n' + text)
          })
        })
        .catch(err => {
          console.warn('api error: ' + err.message)
        })
  },

  checkError (res) {
    if (!res.ok) return res.text().then(text => {
      throw new Error('error ' + res.status + '\n' + text)
    });
    return res
  },

  getArticles (page, limit) {
    return this
        .apiRequest(`/?limit=${limit}&page=${page}&status=active`)
        .then(res => res.json())
        .catch(this.checkError)
  },

  getArticleById (id) {
    return this
        .apiRequest(`/${id}`)
        .then(res => res.json())
        .catch(this.checkError)
  },

  createArticle(data){
    return this
        .apiRequest(`/`,
            {
              method: 'POST',
              headers: {'Content-Type': 'application/json'},
              body: JSON.stringify(data)
            }
        )
        .catch(this.checkError)
  },

  updateArticle(id, body){
    return this
        .apiRequest(`/${id}`,
            {
              method: 'PUT',
              headers: {'Content-Type': 'application/json'},
              body: JSON.stringify(body)
            }
        )
        .then(() => this.getArticleById(id))
        .catch(this.checkError)
  }
}