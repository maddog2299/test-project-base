'use strict'
require('../styles/main.scss');

import React from 'react';
import {connect} from 'react-redux';

class MainPageComponent extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      message: 'wow!'
    }
  }

  render() {
    return (
        <div>
          <h1>{this.state.message}</h1>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    message: state.message
  }
}

export default connect(mapStateToProps)(MainPageComponent)