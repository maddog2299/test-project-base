'use strict'
require('../styles/main.scss');

import React from 'react';
import {connect} from 'react-redux';

class Main extends React.Component {

  constructor(props){
    super(props);
    this.state = {}
  }

  render() {
    return (
        <div>
          {this.props.children}
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    message: state.message
  }
}

export default connect(mapStateToProps)(Main)