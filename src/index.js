'use strict'

import React from 'react';
import {render} from 'react-dom';
import {Router, Route, IndexRoute, Switch} from 'react-router';
import App from './components/MainComponent';
import MainPageComponent from './components/MainPageComponent';
import {syncHistoryWithStore} from 'react-router-redux';
import {Provider} from 'react-redux';
import store from './redux/store';
import { createBrowserHistory } from 'history';
const history = syncHistoryWithStore(createBrowserHistory(), store);

render((
    <Provider store={store}>
      <Router history={history}>
        <App>
          <Switch>
            <Route exact path="/" component={MainPageComponent} />
          </Switch>
        </App>
      </Router>
    </Provider>
), document.getElementById('main'));